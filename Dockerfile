FROM node:16-alpine

WORKDIR /app

COPY . .

RUN apk add git

RUN npm install --force

EXPOSE 3000

CMD ["npm", "start"]
