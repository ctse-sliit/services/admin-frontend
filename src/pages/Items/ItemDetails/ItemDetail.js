import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {
  Button,
  Card,
  CardBody,
  Col,
  Container,
  Nav,
  NavItem,
  NavLink,
  Row,
  TabContent,
  Input,
  TabPane,
  Table,
} from "reactstrap";
import classnames from "classnames";
import { isEmpty } from "lodash";

//Import Breadcrumb
import Breadcrumbs from "../../../components/Common/Breadcrumb";

//Import Star Ratings
import StarRatings from "react-star-ratings";

//Import actions
import { getProductDetail } from "../../../store/actions";

import Reviews from "./Reviews";

class ItemDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeTab: "1",
      activeDescriptionTab: "description",
      product: {},
      breadcrumbItems: [
        { title: "E-commerce", link: "/dashboard" },
        { title: "Product Detail", link: "#" },
      ],
    };
    this.toggleTab = this.toggleTab.bind(this);
    this.toggledescription = this.toggledescription.bind(this);
    this.imageShow = this.imageShow.bind(this);
  }

  componentDidMount() {
    const {
      match: { params },
      onGetProductDetail,
    } = this.props;
    if (params && params.id) {
      onGetProductDetail(params.id);
    }
  }

  toggleTab(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab,
      });
    }
  }

  toggledescription(tab) {
    if (this.state.activeDescriptionTab !== tab) {
      this.setState({
        activeDescriptionTab: tab,
      });
    }
  }

  imageShow(img, id) {
    const expandImg = document.getElementById("expandedImg" + id);
    expandImg.src = img;
  }

  render() {
    const { product } = this.props;

    return (
      <React.Fragment>
        <div className="page-content">
          <Container fluid>
            <Breadcrumbs
              title="Product Detail"
              breadcrumbItems={this.state.breadcrumbItems}
            />

            {!isEmpty(product) && (
              <React.Fragment>
                <Row>
                  <Col lg={12}>
                    <Card>
                      <CardBody>
                        <Row>
                          <Col xl="5">
                            <div className="product-detail">
                              <Row>
                                <Col xs="3">
                                  <Nav className="flex-column" pills>
                                    <NavItem>
                                      <NavLink
                                        className={classnames({
                                          active: this.state.activeTab === "1",
                                        })}
                                        onClick={() => {
                                          this.toggleTab("1");
                                        }}
                                      >
                                        <img
                                          src={product.image}
                                          alt=""
                                          onClick={() => {
                                            this.imageShow(product.image, 1);
                                          }}
                                          className="img-fluid mx-auto d-block tab-img rounded"
                                        />
                                      </NavLink>
                                    </NavItem>
                                    <NavItem>
                                      <NavLink
                                        className={classnames({
                                          active: this.state.activeTab === "2",
                                        })}
                                        onClick={() => {
                                          this.toggleTab("2");
                                        }}
                                      >
                                        <img
                                          src={product.extraimgs[0]}
                                          alt=""
                                          onClick={() => {
                                            this.imageShow(
                                              product.extraimgs[0],
                                              2
                                            );
                                          }}
                                          className="img-fluid mx-auto d-block tab-img rounded"
                                        />
                                      </NavLink>
                                    </NavItem>
                                    <NavItem>
                                      <NavLink
                                        className={classnames({
                                          active: this.state.activeTab === "3",
                                        })}
                                        onClick={() => {
                                          this.toggleTab("3");
                                        }}
                                      >
                                        <img
                                          src={product.extraimgs[1]}
                                          alt=""
                                          onClick={() => {
                                            this.imageShow(
                                              product.extraimgs[1],
                                              3
                                            );
                                          }}
                                          className="img-fluid mx-auto d-block tab-img rounded"
                                        />
                                      </NavLink>
                                    </NavItem>
                                    <NavItem>
                                      <NavLink
                                        className={classnames({
                                          active: this.state.activeTab === "4",
                                        })}
                                        onClick={() => {
                                          this.toggleTab("4");
                                        }}
                                      >
                                        <img
                                          src={product.extraimgs[2]}
                                          alt=""
                                          onClick={() => {
                                            this.imageShow(
                                              product.extraimgs[2],
                                              4
                                            );
                                          }}
                                          className="img-fluid mx-auto d-block tab-img rounded"
                                        />
                                      </NavLink>
                                    </NavItem>
                                  </Nav>
                                </Col>
                                <Col xs="9">
                                  <TabContent
                                    activeTab={this.state.activeTab}
                                    className="position-relative"
                                  >
                                    <TabPane tabId="1">
                                      <div className="product-img">
                                        <img
                                          src={product.image}
                                          alt=""
                                          id="expandedImg1"
                                          className="img-fluid mx-auto d-block"
                                        />
                                      </div>
                                    </TabPane>
                                    <TabPane tabId="2">
                                      <div className="product-img">
                                        <img
                                          src={product.image}
                                          id="expandedImg2"
                                          alt=""
                                          className="img-fluid mx-auto d-block"
                                        />
                                      </div>
                                    </TabPane>
                                    <TabPane tabId="3">
                                      <div className="product-img">
                                        <img
                                          src={product.image}
                                          id="expandedImg3"
                                          alt=""
                                          className="img-fluid mx-auto d-block"
                                        />
                                      </div>
                                    </TabPane>
                                    <TabPane tabId="4">
                                      <div className="product-img">
                                        <img
                                          src={product.image}
                                          id="expandedImg4"
                                          alt=""
                                          className="img-fluid mx-auto d-block"
                                        />
                                      </div>
                                    </TabPane>
                                  </TabContent>
                                  <Row className="text-center mt-2">
                                    <div className="">
                                      <div className="d-grid">
                                        {"Pending" === "Pending" ? (
                                          <span>
                                            <Button
                                              type="button"
                                              color="primary"
                                              className="btn-block waves-effect waves-light mt-2 me-1"
                                            >
                                              <i className="uil uil-shopping-cart-alt me-2"></i>{" "}
                                              Change Status
                                            </Button>
                                          </span>
                                        ) : null}
                                        {/* <Button
                                          type="button"
                                          color="primary"
                                          className="btn-block waves-effect waves-light mt-2 me-1"
                                        >
                                          <i className="uil uil-shopping-cart-alt me-2"></i>{" "}
                                          Change Status
                                        </Button> */}
                                      </div>
                                    </div>
                                  </Row>
                                </Col>
                              </Row>
                            </div>
                          </Col>

                          <Col xl="7">
                            <div className="mt-4 mt-xl-3">
                              <Link to="#" className="text-primary">
                                {product.category}
                              </Link>
                              <h5 className="mt-1 mb-3">{product.name}</h5>

                              <h5 className="mt-2">
                                ${product.newprice}
                              </h5>

                              <p className="mt-3">{product.description}</p>

                              <hr className="my-4" />

                              <Row>
                                <Col md="6">
                                  <div>
                                    {/* <h5 className="font-size-14">
                                      <i className="mdi mdi-location"></i>{" "}
                                      Delivery location
                                    </h5>
                                    <div className="d-flex flex-wrap">
                                      <div className="input-group mb-3 w-auto">
                                        <Input
                                          type="text"
                                          className="form-control"
                                          placeholder="Enter Delivery pincode"
                                        />
                                        <button
                                          className="btn btn-light"
                                          type="button"
                                        >
                                          Check
                                        </button>
                                      </div>
                                    </div> */}

                                    <h5 className="font-size-14">
                                      Specification :
                                    </h5>
                                    <ul className="list-unstyled product-desc-list">
                                      {product.shortspecifications &&
                                        product.shortspecifications.map(
                                          (item, i) => (
                                            <li key={i}>
                                              <i className="mdi mdi-circle-medium me-1 align-middle"></i>{" "}
                                              {item}
                                            </li>
                                          )
                                        )}
                                    </ul>
                                  </div>
                                </Col>

                                <Col md="6">
                                  {/* <h5 className="font-size-14">Services :</h5>
                                  <ul className="list-unstyled product-desc-list">
                                    {product.shortservices &&
                                      product.shortservices.map((item, i) => (
                                        <li key={i}>
                                          <i
                                            className={
                                              "mdi " +
                                              item.icon +
                                              " text-primary me-1 font-size-16"
                                            }
                                          ></i>{" "}
                                          {item.value}
                                        </li>
                                      ))}
                                  </ul> */}
                                </Col>
                              </Row>
                            </div>
                          </Col>
                        </Row>
                      </CardBody>
                    </Card>
                  </Col>
                </Row>
              </React.Fragment>
            )}
          </Container>
        </div>
      </React.Fragment>
    );
  }
}

ItemDetail.propTypes = {
  product: PropTypes.object,
  match: PropTypes.object,
  onGetProductDetail: PropTypes.func,
};

const mapStateToProps = ({ Ecommerce }) => ({
  product: Ecommerce.product,
});

const mapDispatchToProps = (dispatch) => ({
  onGetProductDetail: (id) => dispatch(getProductDetail(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetail);
