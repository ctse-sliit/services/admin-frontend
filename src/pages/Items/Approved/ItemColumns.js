import React from "react"
import { Link } from "react-router-dom"

const ItemColumns = () => [
  {
    dataField: "username",
    text: "Item Name",
    sort: true,
  },
  {
    dataField: "",
    text: "Price",
    sort: true,
  },
  {
    dataField: "",
    text: "Quantity",
    sort: true,
  },
  {
    text: "Category",
    dataField: "Category",
    sort: true,
  },
  {
    dataField: "",
    text: "Best By",
    sort: true,
  },
  {
    dataField: "menu",
    isDummyField: true,
    text: "Action",
    formatter: () => (
      <>
        <Link to="/itemDetail/1" className="me-3 text-primary"><i className="mdi mdi-eye font-size-18"></i></Link>
        <Link to="#" className="text-danger"><i className="mdi mdi-trash-can font-size-18"></i></Link>
      </>
    ),
  },
]

export default ItemColumns
