import React, { Fragment } from "react";
import { Link } from "react-router-dom";
import {
  UncontrolledDropdown,
  DropdownMenu,
  DropdownItem,
  DropdownToggle,
  Badge,
  CardBody,
  CardText,
  Card,
  CardHeader,
  CardTitle,
  FormGroup,
  Label,
  Input,
  UncontrolledButtonDropdown,
} from "reactstrap";
import {
  MoreVertical,
  Edit,
  Trash,
  Share,
  FileText,
  ChevronDown,
} from "react-feather";

const handleUpdateStatus = (item) => {
  console.log(item);
};

const ItemColumns = () => [
  {
    dataField: "seller",
    text: "Seller",
    sort: true,
  },
  {
    dataField: "buyer",
    text: "Buyer",
    sort: true,
  },
  {
    text: "address",
    dataField: "address",
    sort: true,
  },
  {
    dataField: "totalPrice",
    text: "totalPrice",
    sort: true,
  },
  {
    text: "Status",
    dataField: "status",
    sort: true,
    minWidth: "150px",
    cell: (row) => {
      let badgeColor;
      let badgeText;

      if (row.status === "pending") {
        badgeColor = "warning";
        badgeText = "Pending";
      } else if (row.status === "Approved") {
        badgeColor = "success";
        badgeText = "Approved";
      } else {
        badgeColor = "danger";
        badgeText = "Denied";
      }

      return (
        <Badge color={badgeColor} className="badge-glow">
          {badgeText}
        </Badge>
      );
    },
  },
  {
    text: "Action",
    formatter: (item) => (
      <Fragment>
        <UncontrolledDropdown>
          <DropdownToggle
            className="fas fa-ellipsis-v"
            color="transparent"
            size="sm"
            caret
          >
            <MoreVertical size={15} />
          </DropdownToggle>
          <DropdownMenu right>
            <DropdownItem
              className="w-100"
            >
              <Edit className="mr-3" size={10} />
              <span className="align-middle">Update Status</span>
            </DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
      </Fragment>
    ),
  }
  
];

export default ItemColumns;
